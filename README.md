**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).


## Command line stash, pull, merge , commit and push steps

Please follow below steps to push the changes to remote repository once you added or modified any file in your local. 

1.git stash - This command is used to save the local changes to your local repository

2.git pull - This command is used to pull the latest form the remote repository

3.git stash pop - This will merge your local and remote changes in your local repository

4.git add <filename1, filename2,....>  - You can add one or more number of changed files into your local repository to be commit.

5.git commit -m '<Any comments to be added which is related your change>'
  Ex: git commit -m 'This is my first commit'

6.git push - This is the final step to push the changes to your remote repository.
             The commitsare always will be always in your local repository unless you execute the push command.

Check the local repository changes:
1.git status - This is the command you can use to check your local changes in your repository.

## Remove or delete any files from repository

1.git rm <filename> - This will delete your file first from the local repository

2.git commit -m '<Reason for deleting the file>'

3.git push

## change Branch from current to other

1.git checkout <Branch Name> or git change_branch <Branch Name>

Save your current branch changes using git stash command before toggling the branges.

## Replace your file with remote file - If you don't want any local changes and want to your local file identical with your remote file

1.git checkout <Filename> or

2.git reset HEAD <FileName>	
