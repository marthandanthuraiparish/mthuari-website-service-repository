package com.mthurai.web.json.storage;

/**
 * Created by dev on 6/25/20.
 */
public interface FileManager {

    void writeFile(String folderName, String fileName, String content);

    String readFile(String folderName, String fileName);

    void deleteFile(String folderName, String fileName);

    boolean isFileExists(String folderName, String fileName);
}
