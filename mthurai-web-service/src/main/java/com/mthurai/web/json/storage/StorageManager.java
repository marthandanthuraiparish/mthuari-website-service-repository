package com.mthurai.web.json.storage;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mthurai.web.json.ObjectMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * Created by dev on 6/18/20.
 */
@Component
public class StorageManager {

    private final ObjectMapper objectMapper = ObjectMapperFactory.objectMapper();

    private FileManager fileManager;

    @Autowired
    private LocalizedFileManager localizedFileManager;

    @Autowired
    private AmazonS3Manager amazonS3Manager;

    @Value("${amazonS3.storage.enabled:false}")
    private boolean isS3StorageEnable;

    @PostConstruct
    private void initialize() throws IOException {
        if(isS3StorageEnable) {
            fileManager= amazonS3Manager;
        } else {
            fileManager= localizedFileManager;
        }
    }

    public void writeJson(String folderName, String fileName, String value) {

        try {
            fileManager.writeFile(folderName, fileName, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String readJson(String folderName, String fileName) {

        try {
            return fileManager.readFile(folderName, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
