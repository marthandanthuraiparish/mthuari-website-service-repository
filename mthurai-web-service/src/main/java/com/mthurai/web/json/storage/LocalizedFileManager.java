package com.mthurai.web.json.storage;

import com.mthurai.web.utils.Constants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by dev on 6/16/20.
 */
@Component
public class LocalizedFileManager implements FileManager {

    @Value("${local.storage.root.path}")
    private String localStoragePath;

    @PostConstruct
    private void initialize() throws IOException {
        Files.createDirectories(getFilePath(null, null));
        Files.createDirectories(getFilePath(Constants.JSON_FILES_FOLDER, null));
        Files.createDirectories(getFilePath(Constants.IMG_FILES_FOLDER, null));
        Files.createDirectories(getFilePath(Constants.VIDEO_FILES_FOLDER, null));
    }

    @Override
    public void writeFile(String folderName, String fileName, String content) {

        try {
            Path path = getFilePath(folderName, fileName);
            Files.createDirectories(path.getParent());
            if(!Files.exists(path)) {
                Files.createFile(path);
            }
            Files.write(path, content.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String readFile(String folderName, String fileName) {

        try {
            Path path = getFilePath(folderName, fileName);
            if(Files.exists(path)) {
                return new String(Files.readAllBytes(path));
            }
        } catch (NoSuchFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean isFileExists(String folderName, String fileName) {
        Path path = getFilePath(folderName, fileName);
        return Files.exists(path);
    }

    @Override
    public void deleteFile(String folderName, String fileName) {

    }

    private Path getFilePath(String folderName, String fileName) {

        StringBuilder sb =new StringBuilder(localStoragePath);
        if(!StringUtils.isEmpty(folderName)) {
            sb.append(Constants.FILE_SUFFIX).append(folderName);
        }
        if (!StringUtils.isEmpty(fileName)) {
            sb.append(Constants.FILE_SUFFIX).append(fileName);
        }
        return Paths.get(sb.toString());
    }

}
