package com.mthurai.web.json.storage;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.mthurai.web.utils.Constants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.InputStream;


/**
 * Created by dev on 6/16/20.
 */
@Component
public class AmazonS3Manager implements FileManager {

    private AmazonS3 amazonS3Client;

    @Value("${amazonProperties.endpointUrl}")
    private String endpointUrl;
    @Value("${amazonProperties.bucketName}")
    private String bucketName;
    @Value("${amazonProperties.accessKey}")
    private String accessKey;
    @Value("${amazonProperties.secretKey}")
    private String secretKey;

    @Value("${amazonS3.storage.enabled:false}")
    private boolean isS3StorageEnable;

    @PostConstruct
    private void initializeAmazon() {

        if(isS3StorageEnable) {
            AWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
            this.amazonS3Client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.AP_SOUTH_1).build();

            if (!amazonS3Client.doesBucketExistV2(bucketName)) {
                amazonS3Client.createBucket(bucketName);
            }
        }
    }

    @Override
    public void writeFile(String folderName, String fileName, String content) {

        try {

            String key = getObjectKey(folderName, fileName);

            byte[] byteContent = content.getBytes();

            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(byteContent.length);

            InputStream objectContent = new ByteArrayInputStream(byteContent);

            PutObjectRequest request = new PutObjectRequest(bucketName, key, objectContent,
                                                            objectMetadata).withCannedAcl(
                    CannedAccessControlList.PublicReadWrite);

            amazonS3Client.putObject(request);


        } catch (AmazonClientException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @folderName - The value of this argument should be the full folder path excluding the bucket name
     * @fileName - The value of this argument is only the file name with extension without any parent folder
     */
    @Override
    public String readFile(String folderName, String fileName) {

        try {

            if (isFileExists(folderName, fileName)) {
                return amazonS3Client.getObjectAsString(bucketName, getObjectKey(folderName, fileName));
            }

        } catch (AmazonClientException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * @folderName - The value of this argument should be the full folder path excluding the bucket name
     * @fileName - The value of this argument is only the file name with extension without any parent folder
     */
    @Override
    public boolean isFileExists(String folderName, String fileName) {
        return amazonS3Client.doesObjectExist(bucketName, getObjectKey(folderName, fileName));
    }

    @Override
    public void deleteFile(String folderName, String fileName) {

    }


    /**
     * @folderName The value of this argument should be the full folder path excluding the bucket name
     */
    public String[] listAllObjects(String folderName) {

        try {

            ObjectListing objectListing = getAllObjects(folderName);

            if (objectListing != null && !objectListing.getObjectSummaries().isEmpty()) {

                String[] strArray = new String[objectListing.getObjectSummaries().size()];
                int i =0;
                for (S3ObjectSummary s3ObjectSummary : objectListing.getObjectSummaries()) {

                    String arr[] = s3ObjectSummary.getKey().split("/");
                    strArray[i] = readFile(arr[0], arr[1]);
                    i++;
                }
                return strArray;
            }
        } catch (AmazonClientException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private ObjectListing getAllObjects(String folderName) {

        try {

            return amazonS3Client.listObjects(
                    new ListObjectsRequest().withBucketName(bucketName).withPrefix(folderName));
        } catch (AmazonClientException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private String getObjectKey(String folderName, String fileName) {
        StringBuilder sb =new StringBuilder(folderName);
        sb.append(Constants.FILE_SUFFIX).append(fileName);
        return sb.toString();
    }

}
