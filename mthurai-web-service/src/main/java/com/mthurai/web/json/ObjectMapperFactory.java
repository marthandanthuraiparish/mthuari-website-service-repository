package com.mthurai.web.json;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import org.springframework.stereotype.Component;

/**
 * Build an ObjectMapper for serialising/de-serialising Json.
 *
 */
@Component
public final class ObjectMapperFactory {

    private ObjectMapperFactory() {
    }

    /**
     * Essentially just an alias to ObjectMapper to aid debugging, if you see an instance of TheRealSlimObjectMapper you know its the right one.
     */
    public static class TheRealSlimObjectMapper extends ObjectMapper {

    }

    public static ObjectMapper build() {
        ObjectMapper objectMapper = new TheRealSlimObjectMapper()
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        objectMapper.setVisibility(VisibilityChecker.Std.defaultInstance()
            .withFieldVisibility(JsonAutoDetect.Visibility.ANY));

        return objectMapper;
    }

    public static ObjectMapper objectMapper() {
        return build();
    }

}

