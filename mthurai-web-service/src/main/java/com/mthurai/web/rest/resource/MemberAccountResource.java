package com.mthurai.web.rest.resource;

import com.mthurai.web.dto.LoginRequest;
import com.mthurai.web.dto.MemberAccountDto;
import com.mthurai.web.service.MemberAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by dev on 6/9/20.
 */
@RestController
@RequestMapping("/memberAccount")
public class MemberAccountResource {

    @Autowired
    private MemberAccountService memberAccountService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<MemberAccountDto> login(@RequestBody LoginRequest loginRequest) {
        MemberAccountDto memberAccountDto = memberAccountService.getByLoginName(loginRequest.getLoginName());
        if (memberAccountDto != null && memberAccountDto.getLoginPassword().equals(loginRequest.getLoginName())) {
            return ResponseEntity.ok(memberAccountDto);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/listMembers", method = RequestMethod.GET)
    public ResponseEntity<List<MemberAccountDto>> list() {
        return ResponseEntity.ok(memberAccountService.getAllMemberAccounts());
    }

    @RequestMapping(value = "/createMember", method = RequestMethod.POST)
    public ResponseEntity<MemberAccountDto> createMemberAccount(@RequestBody MemberAccountDto memberAccountDto) {
        MemberAccountDto savedMember = memberAccountService.add(memberAccountDto);
        if (savedMember != null) {
            return ResponseEntity.ok(savedMember);
        }
        return new ResponseEntity(HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/updateMember", method = RequestMethod.PUT)
    public ResponseEntity<MemberAccountDto> updateMemberAccount(@RequestBody MemberAccountDto memberAccountDto) {
        MemberAccountDto savedMember = memberAccountService.modify(memberAccountDto);
        if (savedMember != null) {
            return ResponseEntity.ok(savedMember);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/verifyMember/{loginName}", method = RequestMethod.PUT)
    public ResponseEntity<MemberAccountDto> verifyMemberAccount(@PathVariable String loginName) {
        MemberAccountDto savedMember = memberAccountService.verify(loginName);
        if (savedMember != null) {
            return ResponseEntity.ok(savedMember);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
}
