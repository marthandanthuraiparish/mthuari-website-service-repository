package com.mthurai.web.filter;

import com.mthurai.web.dto.APIError;
import com.mthurai.web.utils.Constants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by dev on 7/6/20.
 */
@Component
@Order(1)
public class APIResourceAuthenticationFilter extends OncePerRequestFilter {

    private static final String USERNAME_KEY = Constants.SPRING_SECURITY_FORM_USERNAME_KEY;
    private static final String PASSWORD_KEY = Constants.SPRING_SECURITY_FORM_PASSWORD_KEY;

    @Value("${spring.security.user.name}")
    private String restUserName;
    @Value("${spring.security.user.password}")
    private String restPassword;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException{

        String username = httpServletRequest.getHeader(USERNAME_KEY);
        String password = httpServletRequest.getHeader(PASSWORD_KEY);

        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            setErrorResponse(HttpStatus.BAD_REQUEST, httpServletResponse, new BadCredentialsException("Required headers (username and password) not specified in the request"));
            return;
        }

        if(!(restUserName.equals(username) && restPassword.equals(password))) {
            setErrorResponse(HttpStatus.UNAUTHORIZED, httpServletResponse, new BadCredentialsException("Invalid API user Credentials"));
            return;
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    public void setErrorResponse(HttpStatus status, HttpServletResponse response, Throwable ex){
        response.setStatus(status.value());
        response.setContentType(MediaType.APPLICATION_JSON.getType());
        APIError apiError = new APIError(status, ex);
        try {
            String json = apiError.convertToJson();
            response.getWriter().write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
