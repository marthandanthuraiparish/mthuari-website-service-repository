package com.mthurai.web.service;

import com.mthurai.web.dto.MemberAccountDto;

import java.util.List;

/**
 * Created by dev on 6/9/20.
 */
public interface MemberAccountService {
    MemberAccountDto add(MemberAccountDto memberAccountDto);
    MemberAccountDto modify(MemberAccountDto memberAccountDto);
    MemberAccountDto getByLoginName(String loginName);
    MemberAccountDto verify(String accountId);
    List<MemberAccountDto> getAllMemberAccounts();
}
