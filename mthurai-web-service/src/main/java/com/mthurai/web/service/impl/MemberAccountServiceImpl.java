package com.mthurai.web.service.impl;

import com.mthurai.web.dao.MemberAccountDAO;
import com.mthurai.web.dto.MemberAccountDto;
import com.mthurai.web.service.MemberAccountService;
import com.mthurai.web.utils.Constants;
import com.mthurai.web.utils.MemberRole;
import com.mthurai.web.utils.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by dev on 6/9/20.
 */
@Service
public class MemberAccountServiceImpl implements MemberAccountService {

    @Autowired
    private MemberAccountDAO memberAccountDAO;

    @Override
    public MemberAccountDto add(MemberAccountDto memberAccountDto) {

        List<MemberAccountDto> accountDtoList = getAllMemberAccounts();

        MemberAccountDto existingMember = filterByLoginName(memberAccountDto.getLoginName(), accountDtoList);

        if (existingMember == null) {
            int maxAccountId = accountDtoList.size();
            memberAccountDto.setAccountId(String.valueOf(maxAccountId + 1));
            setDefaultValues(memberAccountDto, Constants.SAVE);
            accountDtoList.add(memberAccountDto);
            memberAccountDAO.saveOrUpdate(accountDtoList);
            return memberAccountDto;
        }
        return null;
    }

    @Override
    public MemberAccountDto modify(MemberAccountDto memberAccountDto) {

        List<MemberAccountDto> accountDtoList = getAllMemberAccounts();

        MemberAccountDto existingMember = filterByLoginName(memberAccountDto.getLoginName(), accountDtoList);

        if (existingMember != null) {
            memberAccountDto.setAccountId(existingMember.getAccountId());
            accountDtoList.remove(existingMember);
            setDefaultValues(memberAccountDto, Constants.UPDATE);
            accountDtoList.add(memberAccountDto);
            memberAccountDAO.saveOrUpdate(accountDtoList);
            return memberAccountDto;
        }
        return null;
    }

    @Override
    public MemberAccountDto verify(String memberLoginName) {

        List<MemberAccountDto> accountDtoList = getAllMemberAccounts();

        MemberAccountDto existingMember = filterByLoginName(memberLoginName, accountDtoList);

        if (existingMember != null) {
            accountDtoList.remove(existingMember);
            setDefaultValues(existingMember, Constants.VERIFY);
            accountDtoList.add(existingMember);
            memberAccountDAO.saveOrUpdate(accountDtoList);
            return existingMember;
        }
        return null;
    }

    @Override
    public MemberAccountDto getByLoginName(String loginName) {
        List<MemberAccountDto> accountDtoList = getAllMemberAccounts();
        return filterByLoginName(loginName, accountDtoList);
    }

    @Override
    public List<MemberAccountDto> getAllMemberAccounts() {
        return memberAccountDAO.findAll();
    }

    private MemberAccountDto filterByLoginName(String loginName, List<MemberAccountDto> memberAccountList) {

        if (!CollectionUtils.isEmpty(memberAccountList)) {

            Optional<MemberAccountDto> matchingObject = memberAccountList.stream().
                    filter(member -> member.getLoginName().equals(loginName)).findFirst();

            return matchingObject != null && matchingObject.isPresent() ? matchingObject.get() : null;
        }
        return null;
    }

    private void setDefaultValues(MemberAccountDto memberAccountDto, String action) {

        switch (action) {

            case Constants.SAVE:
                memberAccountDto.setMemberRole(MemberRole.getValue(memberAccountDto.getMemberRole()));
                memberAccountDto.setStatus(Status.getValue(memberAccountDto.getStatus()));
                memberAccountDto.setCreatedOn(LocalDateTime.now().toString());
                break;
            case Constants.UPDATE:
                memberAccountDto.setMemberRole(MemberRole.getValue(memberAccountDto.getMemberRole()));
                memberAccountDto.setStatus(Status.getValue(memberAccountDto.getStatus()));
                memberAccountDto.setUpdatedOn(LocalDateTime.now().toString());
                break;
            case Constants.VERIFY:
                memberAccountDto.setVerifiedOn(LocalDateTime.now().toString());
                memberAccountDto.setUpdatedOn(LocalDateTime.now().toString());
                break;
            default:

        }
    }
}
