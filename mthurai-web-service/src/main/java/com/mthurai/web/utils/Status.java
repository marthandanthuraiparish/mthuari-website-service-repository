package com.mthurai.web.utils;

/**
 * Created by dev on 6/29/20.
 */
public enum Status {

    ACTIVE("ACTIVE"),
    INACTIVE("INACTIVE");

    private String value;

    private Status(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static String getValue(String statusValue) {

        for (Status status : values()) {
            if (status.getValue().equalsIgnoreCase(statusValue)) {
                return status.getValue();
            }
        }
        return INACTIVE.getValue();
    }
}
