package com.mthurai.web.utils;

/**
 * Created by dev on 6/9/20.
 */
public final class Constants {

    public static final String SPRING_SECURITY_FORM_USERNAME_KEY = "username";
    public static final String SPRING_SECURITY_FORM_PASSWORD_KEY = "password";

    public static final String SAVE = "save";
    public static final String UPDATE = "update";
    public static final String VERIFY = "verify";
    public static final String FILE_SUFFIX = "/";
    public static final String FILE_EXTN = ".json";
    public static final String JSON_FILES_FOLDER = "json-files";
    public static final String IMG_FILES_FOLDER = "image-files";
    public static final String VIDEO_FILES_FOLDER = "video-files";

    public static final String MEMBER_ACCOUNT_FILE_NAME = "members" + FILE_EXTN;




}
