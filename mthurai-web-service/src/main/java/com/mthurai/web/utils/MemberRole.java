package com.mthurai.web.utils;

/**
 * Created by dev on 6/29/20.
 */

public enum MemberRole {

    USER("USER"),
    ADMIN("ADMIN");

    private String value;

    private MemberRole(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static String getValue(String role) {

        for (MemberRole memberRole : values()) {
            if (memberRole.getValue().equalsIgnoreCase(role)) {
                return memberRole.getValue();
            }
        }
        return USER.getValue();
    }


}
