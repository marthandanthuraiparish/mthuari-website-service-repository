package com.mthurai.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by dev on 6/9/20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberAccountDto extends BaseDto {

    @JsonProperty("accountId")
    private String accountId;
    @JsonProperty("loginName")
    private String loginName;
    @JsonProperty("loginPassword")
    private String loginPassword;
    @JsonProperty("memberRole")
    private String memberRole;
    @JsonProperty("status")
    private String status;

    @JsonProperty("memberName")
    private String memberName;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("dateOfBirth")
    private String dateOfBirth;
    @JsonProperty("fatherName")
    private String fatherName;
    @JsonProperty("motherName")
    private String motherName;
    @JsonProperty("spouseName")
    private String spouseName;
    @JsonProperty("homeAddress")
    private String homeAddress;
    @JsonProperty("currentLocation")
    private String currentLocation;
    @JsonProperty("parishName")
    private String parishName;
    @JsonProperty("parishUnitNo")
    private String parishUnitNo;
    @JsonProperty("maritalStatus")
    private String maritalStatus;
    @JsonProperty("marriedOn")
    private String marriedOn;
    @JsonProperty("noOfDependants")
    private String noOfDependants;
    @JsonProperty("bloodGroup")
    private String bloodGroup;
    @JsonProperty("showBloodGroup")
    private String showBloodGroup;
    @JsonProperty("mobileNo")
    private String mobileNo;
    @JsonProperty("showMobileNo")
    private String showMobileNo;
    @JsonProperty("emailAddress")
    private String emailAddress;
    @JsonProperty("showEmail")
    private String showEmail;
    @JsonProperty("school")
    private String school;
    @JsonProperty("higherEducation")
    private String higherEducation;
    @JsonProperty("companyName")
    private String companyName;
    @JsonProperty("natureOfJob")
    private String natureOfJob;
    @JsonProperty("jobLocation")
    private String jobLocation;
    @JsonProperty("verifiedBy")
    private String verifiedBy;
    @JsonProperty("verifiedOn")
    private String verifiedOn;


    public MemberAccountDto() {
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getMemberRole() {
        return memberRole;
    }

    public void setMemberRole(String memberRole) {
        this.memberRole = memberRole;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getParishName() {
        return parishName;
    }

    public void setParishName(String parishName) {
        this.parishName = parishName;
    }

    public String getParishUnitNo() {
        return parishUnitNo;
    }

    public void setParishUnitNo(String parishUnitNo) {
        this.parishUnitNo = parishUnitNo;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMarriedOn() {
        return marriedOn;
    }

    public void setMarriedOn(String marriedOn) {
        this.marriedOn = marriedOn;
    }

    public String getNoOfDependants() {
        return noOfDependants;
    }

    public void setNoOfDependants(String noOfDependants) {
        this.noOfDependants = noOfDependants;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getShowBloodGroup() {
        return showBloodGroup;
    }

    public void setShowBloodGroup(String showBloodGroup) {
        this.showBloodGroup = showBloodGroup;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getShowMobileNo() {
        return showMobileNo;
    }

    public void setShowMobileNo(String showMobileNo) {
        this.showMobileNo = showMobileNo;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getShowEmail() {
        return showEmail;
    }

    public void setShowEmail(String showEmail) {
        this.showEmail = showEmail;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getHigherEducation() {
        return higherEducation;
    }

    public void setHigherEducation(String higherEducation) {
        this.higherEducation = higherEducation;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getNatureOfJob() {
        return natureOfJob;
    }

    public void setNatureOfJob(String natureOfJob) {
        this.natureOfJob = natureOfJob;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public String getVerifiedOn() {
        return verifiedOn;
    }

    public void setVerifiedOn(String verifiedOn) {
        this.verifiedOn = verifiedOn;
    }
}
