package com.mthurai.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by dev on 6/29/20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginRequest implements Serializable {

    @JsonProperty("loginName")
    private String loginName;
    @JsonProperty("loginPassword")
    private String loginPassword;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }
}
