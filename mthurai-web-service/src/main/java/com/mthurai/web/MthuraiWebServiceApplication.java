package com.mthurai.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MthuraiWebServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MthuraiWebServiceApplication.class, args);
	}
}
