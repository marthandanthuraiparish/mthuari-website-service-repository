package com.mthurai.web.dao.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mthurai.web.dao.MemberAccountDAO;
import com.mthurai.web.dto.MemberAccountDto;
import com.mthurai.web.json.ObjectMapperFactory;
import com.mthurai.web.json.storage.StorageManager;
import com.mthurai.web.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dev on 6/9/20.
 */
@Component
public class MemberAccountDAOImpl implements MemberAccountDAO {

    private final ObjectMapper objectMapper = ObjectMapperFactory.objectMapper();

    @Autowired
    private StorageManager storageManager;

    private static final String FOLDER_NAME = Constants.JSON_FILES_FOLDER;
    private static final String FILE_NAME = Constants.MEMBER_ACCOUNT_FILE_NAME;

    @Override
    public void saveOrUpdate(List<MemberAccountDto> memberAccounts) {

        try {
            storageManager.writeJson(FOLDER_NAME, FILE_NAME, objectMapper.writeValueAsString(memberAccounts));
        } catch (Exception ie) {
            ie.printStackTrace();
        }
    }

    @Override
    public List<MemberAccountDto> findAll() {

        try {
            String content = storageManager.readJson(FOLDER_NAME, FILE_NAME);
            if (content != null) {
                return objectMapper.readValue(content, new TypeReference<List<MemberAccountDto>>() {
                });
            }
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

}
