package com.mthurai.web.dao;

import com.mthurai.web.dto.MemberAccountDto;

import java.util.List;

/**
 * Created by dev on 6/9/20.
 */
public interface MemberAccountDAO {

    void saveOrUpdate(List<MemberAccountDto> memberAccounts);
    List<MemberAccountDto> findAll();
}
